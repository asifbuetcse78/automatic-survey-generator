# automatic dummy survey generator

Language Used: Java

## Features
- Generate survey response around a given estimate using weighted random number generator.
- Printing the output in text or .sav format for SPSS use.
- Generating seperate ouput for each attribute for easier use in SPSS. 
